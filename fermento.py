#!/usr/bin/env python3
'''
Copyright 2022 Florian Olbrich

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
'''

'''
FermentoPy for FermentoPi
Python App to temperature and humidity control a fermentation chamber (for e.g. making Yoghurt, Tempeh, Sourdough, ...
with high energy efficiency by directly using the excess heat of the raspberry pi while utilizing it to
participate at folding@home :-D
'''
   
from simple_pid import PID
from subprocess import Popen, PIPE, STDOUT
from time import sleep, time
import signal, psutil
import os
from threading import Thread
import csv

import pigpio
import time
from datetime import datetime,timedelta,timezone

import eventlet
import socketio

from queue import Queue, Empty

import json

from smbus2 import SMBus
from bme280 import BME280

#from board import D4
#import adafruit_dht





#
# Process controller stuff
#


def notes():
  pi = pigpio.pi()
  pin = 13
  pi.set_mode(pin, pigpio.ALT5)
  # hardware_PWM(pin, Hz, duy_cycle 0-1000000)
  pi.hardware_PWM(pin, 25000, 500000)   


# https://abyz.me.uk/rpi/pigpio/python.html#hardware_PWM
# 



#sensor = adafruit_dht.DHT22(D4)
bus = SMBus(1)
sensor = BME280(i2c_dev=bus)

pid_workheat = PID(
    Kp=1.5,
    Ki=0.01,
    Kd=0.0,
    sample_time=1.5,
    output_limits=(-10,10),
    setpoint=29)

def send_sig_child_processes(parent_pid, sig=signal.SIGTERM):
    '''
    Solves the problem of nested subprocesses and process groups for many work processes.
    https://stackoverflow.com/questions/3332043/obtaining-pid-of-child-process
    ''' 
    try:
      parent = psutil.Process(parent_pid)
    except psutil.NoSuchProcess:
      return
    children = parent.children(recursive=True)
    for process in children:
      process.send_signal(sig)


class Heater():
  def __init__(self, pin):
    self.pi = pigpio.pi()
    self.pin = pin
    self.pi.set_mode(self.pin, pigpio.OUTPUT)
    self.pi.set_PWM_frequency(17,1)
    self.set_power(0)

  def set_power(self,power):
    # Set power in percent (0-100)
    self.power=power
    self.pi.set_PWM_dutycycle(self.pin,255/100*power)

class Cooler():
  def __init__(self, pin):
    self.pi = pigpio.pi()
    self.pin = pin
    self.pi.set_mode(self.pin, pigpio.ALT5)
    # hardware_PWM(pin, Hz, duty_cycle 0-1000000)
    self.set_power(0)

  def set_power(self,power):
    # Set power in percent (0-100)
    # Make sure a small duty cycle is always output (0 duty cycle = no signal = full speed)
    self.power=power
    self.pi.hardware_PWM(self.pin, 25000, int((1e6-100)/100*power+100))


class WorkSlicer(Thread):

  # Process interval that is sliced by duty_cycle, in seconds.
  # E.g. 4s and duty_cycle of 0.5 -> 2s run, 2s stopped, repeat
  timebase=4
  duty_cycle=0.5
  work=None
  stop=False

  def __init__(self, work, timebase=4, start_duty=0.5):
    super(WorkSlicer,self).__init__()
    self.timebase=timebase
    self.duty_cycle=start_duty
    self.work=work

  def run(self):
    while not self.stop:
      sleep(self.timebase*self.duty_cycle)
      send_sig_child_processes(self.work.pid, signal.SIGSTOP)
      #os.killpg(os.getpgid(self.work.pid), signal.SIGSTOP)
      sleep(self.timebase*(1-self.duty_cycle))
      if self.duty_cycle > 0:
        send_sig_child_processes(self.work.pid, signal.SIGCONT)
        #os.killpg(os.getpgid(self.work.pid), signal.SIGCONT)


data_history=[]

def process_main(command_queue, dataout_queue):
  '''
  Initialize and run the heat and humidity controllers. Blocks and waits for
  KeyboardInterrupt, then tears down and returns.
  '''

  global data_history

  # Prepare CSV logfile
  with open('pidlog.csv','w') as logfile:
    logwriter = csv.writer(logfile, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    logwriter.writerow(['Time','Timestamp UTC', 'Humidity [%RH]', 'Temperature [°C]', 'Setpoint [°C]', 'Power Workers [%]', 'Power Heater [%]', 'Power Cooler [%]', 'Setpoint [°C]'])



  # Create command fifo (to receive new setpoints)
  try:
    os.mkfifo('/tmp/fermentofifo')
  except FileExistsError:
    pass

  cmdpipe = os.open('/tmp/fermentofifo',os.O_NONBLOCK | os.O_RDONLY)


  # Create own process group for the worker so we can send signals
  # to it and all its child processes
  # Be very nice to not get in the way of other jobs
  worker = Popen(["FAHClient", "--config", "/home/pi/fermentopy/config.xml"], start_new_session=True, preexec_fn=lambda : os.nice(15),stdout=PIPE, stderr=STDOUT, text=True)

  # Spawning a CPU hog to fill up on heating power when FAHClient doesn't get enough work packages to saturate its workers.
  # Set to lowest possible priority to just fill up the gaps left by FAHClient
  worker_gapfill = Popen(["nice", "--adjustment=19", "stress", "-c", "4"], start_new_session=True, preexec_fn=lambda : os.nice(19))

  slicer=WorkSlicer(worker)
  slicer.start()

  slicer_gapfill=WorkSlicer(worker_gapfill)
  slicer_gapfill.start()

  heater=Heater(17)
  cooler=Cooler(13)


  try:
    while True:
      sleep(2)

      try:
        cmd=os.read(cmdpipe,20)
      except BlockingIOError:
        pass

      if cmd:
        cmdarr=cmd.decode('utf-8').rstrip().split(':')
        if cmdarr[0]=="SP" and cmdarr[1]:
          try:
            newsp=float(cmdarr[1])
            print("New SP: ", newsp)
            pid_workheat.setpoint=newsp
          except ValueError:
            print("Invalid argument to SP")

      try:
        web_cmd=command_queue.get(block=False)
        if 'SP' in web_cmd:
            newsp=float(web_cmd['SP'])
            print("New SP: ", newsp)
            pid_workheat.setpoint=newsp
        if 'RESET_HISTORY' in web_cmd:
            data_history=[]
      except Empty:
        pass


      try:
        #temp=sensor.temperature
        #hum=sensor.humidity
        temp=sensor.get_temperature()
        hum=sensor.get_humidity()
        output=pid_workheat(temp)
        if output < 0:
          out_cooler=-1*output
          out_work=0
          out_heater=0
        elif output>5:
          out_work=5
          out_heater=output-5
          out_cooler=0
        else:
          out_work=output
          out_heater=0
          out_cooler=0

        slicer.duty_cycle=out_work/5
        slicer_gapfill.duty_cycle=out_work/5

        heater.set_power(out_heater/5*100)
        cooler.set_power(out_cooler/10*100)
        print("Hum:",hum, "Setpoint:",pid_workheat.setpoint,"Temp:",temp,"W:",out_work,"H:",out_heater,"C:",out_cooler)
        data_history.append({
                           'time': int(datetime.now(tz=timezone.utc).timestamp()*1e6),
                           'humidity': hum,
                           'temperature': temp,
                           'setpoint': pid_workheat.setpoint,
                           'power_worker':out_work,
                           'power_heater':out_heater,
                           'power_cooler':out_cooler})

        dataout_queue.put(json.dumps([data_history[-1]]))

        with open('pidlog.csv','a') as logfile:
          logwriter = csv.writer(logfile, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
          logwriter.writerow(
                [datetime.now(tz=timezone.utc),
                datetime.now(tz=timezone.utc).timestamp(),
                hum,
                temp,
                pid_workheat.setpoint,
                out_work*20,
                out_heater*20,
                out_cooler*10,
                pid_workheat.setpoint
                ])
      except RuntimeError:
        print("RT ERROR")
        pass

      #with open('./fah_out.log','a') as fahlog:
      #  for line in worker.stdout:
      #    fahlog.write(line)


  except KeyboardInterrupt:
    pass


  os.close(cmdpipe)

  slicer.stop=True
  slicer_gapfill.stop=True

  slicer_gapfill.join()
  slicer.join()


  del slicer
  del slicer_gapfill

  pgid = os.getpgid(worker.pid)
  os.killpg(pgid, signal.SIGCONT)
  os.killpg(pgid, signal.SIGTERM)
  sleep(10)
  os.killpg(pgid, signal.SIGKILL)
  sleep(5)
  worker.terminate()
  sleep(1)
  worker.kill()




#
# Webserver stuff
#

#https://tutorialedge.net/python/python-socket-io-tutorial/
#https://python-socketio.readthedocs.io/en/latest/server.html

# Note - example with flask and background Thread
# https://github.com/miguelgrinberg/python-socketio/blob/main/examples/server/wsgi/app.py

# selbes Problem:
# https://stackoverflow.com/questions/59088098/getting-results-from-queue-to-emit-to-socket-io-correctly


command_queue=Queue()  # Command queue for process controller to send it e.g. new setpoints
dataout_queue=Queue(maxsize=50)  # Data reporting queue out of process controller


#sio = socketio.Server(cors_allowed_origins=['*'])
sio = socketio.Server()
app = socketio.WSGIApp(sio, static_files={
    '/': 'index.html',
    '/index.html': 'index.html',
    '/static/': 'static/',
    '/static/flot/jquery.canvaswrapper.js': 'static/flot/jquery.canvaswrapper.js',
    '/static/flot/jquery.colorhelpers.js': 'static/flot/jquery.colorhelpers.js',
    '/static/flot/jquery.flot.axislabels.js': 'static/flot/jquery.flot.axislabels.js',
    '/static/flot/jquery.flot.browser.js': 'static/flot/jquery.flot.browser.js',
    '/static/flot/jquery.flot.categories.js': 'static/flot/jquery.flot.categories.js',
    '/static/flot/jquery.flot.composeImages.js': 'static/flot/jquery.flot.composeImages.js',
    '/static/flot/jquery.flot.crosshair.js': 'static/flot/jquery.flot.crosshair.js',
    '/static/flot/jquery.flot.drawSeries.js': 'static/flot/jquery.flot.drawSeries.js',
    '/static/flot/jquery.flot.errorbars.js': 'static/flot/jquery.flot.errorbars.js',
    '/static/flot/jquery.flot.fillbetween.js': 'static/flot/jquery.flot.fillbetween.js',
    '/static/flot/jquery.flot.flatdata.js': 'static/flot/jquery.flot.flatdata.js',
    '/static/flot/jquery.flot.hover.js': 'static/flot/jquery.flot.hover.js',
    '/static/flot/jquery.flot.image.js': 'static/flot/jquery.flot.image.js',
    '/static/flot/jquery.flot.js': 'static/flot/jquery.flot.js',
    '/static/flot/jquery.flot.legend.js': 'static/flot/jquery.flot.legend.js',
    '/static/flot/jquery.flot.logaxis.js': 'static/flot/jquery.flot.logaxis.js',
    '/static/flot/jquery.flot.navigate.js': 'static/flot/jquery.flot.navigate.js',
    '/static/flot/jquery.flot.pie.js': 'static/flot/jquery.flot.pie.js',
    '/static/flot/jquery.flot.resize.js': 'static/flot/jquery.flot.resize.js',
    '/static/flot/jquery.flot.saturated.js': 'static/flot/jquery.flot.saturated.js',
    '/static/flot/jquery.flot.selection.js': 'static/flot/jquery.flot.selection.js',
    '/static/flot/jquery.flot.stack.js': 'static/flot/jquery.flot.stack.js',
    '/static/flot/jquery.flot.symbol.js': 'static/flot/jquery.flot.symbol.js',
    '/static/flot/jquery.flot.threshold.js': 'static/flot/jquery.flot.threshold.js',
    '/static/flot/jquery.flot.time.js': 'static/flot/jquery.flot.time.js',
    '/static/flot/jquery.flot.touch.js': 'static/flot/jquery.flot.touch.js',
    '/static/flot/jquery.flot.touchNavigate.js': 'static/flot/jquery.flot.touchNavigate.js',
    '/static/flot/jquery.flot.uiConstants.js': 'static/flot/jquery.flot.uiConstants.js',
    '/static/flot/jquery.js': 'static/flot/jquery.js',
    '/static/socket.io.js': 'static/socket.io.js'
})


@sio.event
def connect(sid, environ):
    print('connect ', sid)

@sio.on('request_history')
def on_message(sid, data):
    global data_history
    print('History requested',sid,data)
    sio.emit('data_history', json.dumps(data_history))

@sio.on('reset_history')
def on_message(sid, data):
    print('History Reset',sid,data)
    command_queue.put({'RESET_HISTORY':True})



    #sio.emit('my message', {'response': 'my response'})
    #sio.emit('my message', 'oleole', room=sid)

@sio.on('set_setpoint')
def on_message(sid, new_sp):
    global sp
    print('New Setpoint from',sid,':',new_sp)
    command_queue.put({'SP':float(new_sp)})

@sio.event
def disconnect(sid):
    print('disconnect ', sid)

def background_acquisition():
    while True:
        sio.sleep(0.1)
        try:
            data = dataout_queue.get(block=False)
            print("FQ: ", data)
            sio.emit('data dict', str(data))
        except Empty:
            pass



def run_webapp():
    ws_server = eventlet.listen(('', 5000))
    ws_thread = sio.start_background_task(background_acquisition)
    eventlet.wsgi.server(ws_server, app)



if __name__ == '__main__':

    # Start webserver
    appsrv = Thread(target=run_webapp, daemon=True)
    appsrv.start()

    process_main(command_queue, dataout_queue)


